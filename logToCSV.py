# -*- coding: utf-8 -*-
"""Module is a CLI utility to extract CSV data from log files
For support: igor.skh@gmail.com (Igor Kim)
01/2018, Leipzig
"""
import os, sys, logging
from evalFunctions.evalFunctions import *
import argparse

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__,
                            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("filepath", help="path to the text log file")
    parser.add_argument("-k","--keyword", required=True, help="keyword to define eval line")
    parser.add_argument("-o","--output", help="path to the output CSV file")
    parser.add_argument("-i","--header", help="initial CSV header")
    parser.add_argument("-l","--delimiter", default="|", help="values delimiter")
    parser.add_argument("-L","--keydelimiter", default=" ", help="key-value delimiter")

    logger = logging.getLogger(__package__)
    logger.setLevel(logging.INFO)    
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)

    args = parser.parse_args()

    filepath = args.filepath
    output = args.output
    header = str(args.header).strip().split() if args.header else []
    keyword = str(args.keyword).strip()
    delimiter = str(args.delimiter).strip()
    kvDelimiter = str(args.keydelimiter)

    logger.info("Converting %s log file to CSV" % filepath)
    parseLogToCSV(filepath, keyword, header, delimiter, kvDelimiter, output)